package com.amalia_10191009.dashboard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashBoardAdaptor extends RecyclerView.Adapter<DashBoardAdaptor.DashBoardHolder> {
    private ArrayList<SetterGeter> listdata ;

    public DashBoardAdaptor(ArrayList<SetterGeter> listdata){
        this.listdata       =   listdata;
    }
    @NonNull
    @Override
    public DashBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view                   = LayoutInflater.from(parent.getContext()). inflate(R.layout.item_dashboard,parent, false);
        DashBoardHolder holder      = new DashBoardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashBoardHolder holder, int position) {

        final SetterGeter getData       =   listdata.get(position);
        String titlemenu        =   getData.getTitle();
        String logomenu          =   getData.getImg();

        holder.titleMenu.setText(titlemenu);
        if (logomenu.equals("logomenu1")){
            holder.imgMenu.setImageResource(R.drawable.transfer);
        }else if (logomenu.equals("logomenu2")){
            holder.imgMenu.setImageResource((R.drawable.ewallet));
        }else if (logomenu.equals("logomenu3")){
            holder.imgMenu.setImageResource(R.drawable.pembayaran);
        }else if (logomenu.equals("logomenu4")){
            holder.imgMenu.setImageResource(R.drawable.pembelian);
        }else if (logomenu.equals("logomenu5")){
            holder.imgMenu.setImageResource(R.drawable.investasi);
        }else if (logomenu.equals("logomenu6")){
            holder.imgMenu.setImageResource(R.drawable.digitalloan);
        }else if (logomenu.equals("logomenu7")){
            holder.imgMenu.setImageResource(R.drawable.creditcard);
        }else if (logomenu.equals("logomenu9")){
            holder.imgMenu.setImageResource(R.drawable.dikado);
        }else if (logomenu.equals("logomenu9")){
            holder.imgMenu.setImageResource(R.drawable.menulengkap);
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashBoardHolder extends RecyclerView.ViewHolder {
        TextView titleMenu;
        ImageView imgMenu;

        public DashBoardHolder(@NonNull View itemView) {
            super(itemView);

            titleMenu       =   itemView.findViewById(R.id.tv_title_transfer);
            imgMenu      =   itemView.findViewById(R.id.iv_menu_transfer);
        }
    }
}
