package com.amalia_10191009.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGeter> datamenu;
    GridLayoutManager   gridLayoutManager   ;
    DashBoardAdaptor adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView            =   findViewById(R.id.rv_menu);

        addData();
        gridLayoutManager       = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter     = new DashBoardAdaptor(datamenu);
        recyclerView.setAdapter(adapter);
    }


    public void addData(){

        datamenu        = new ArrayList<>();
        datamenu.add(new SetterGeter("Transfer", "logomenu1"));
        datamenu.add(new SetterGeter("E-Wallet", "logomenu2"));
        datamenu.add(new SetterGeter("Pembayaran", "logomenu3"));
        datamenu.add(new SetterGeter("Pembelian", "logomenu4"));
        datamenu.add(new SetterGeter("Investasi", "logomenu5"));
        datamenu.add(new SetterGeter("Digital Loan", "logomenu6"));
        datamenu.add(new SetterGeter("My Credit Card", "logomenu7"));
        datamenu.add(new SetterGeter("DiKado", "logomenu8"));
        datamenu.add(new SetterGeter("Menu Lengkap", "logomenu9"));

    }
}